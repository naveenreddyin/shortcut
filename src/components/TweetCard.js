import React from 'react';
import PropTypes from "prop-types";

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import './styles/TweetCardStyles.css'


const TweetCard = (props) => {
    let {tweet} = props

    return (
        <Card className="TweetCard">
            <CardHeader
                avatar={
                    <Avatar aria-label="UserPicture">
                        <img src={tweet.user.profile_image_url} alt={tweet.user.name} />
                    </Avatar>
                }
                title={tweet.user.name}
                subheader={new Date(tweet.created_at).toLocaleTimeString()}
            />
            <CardContent>
                <Typography paragraph>
                    {tweet.text}
                </Typography>
            </CardContent>
        </Card>
    )
}

TweetCard.propTypes = {
    tweet: PropTypes.object.isRequired
}

export default TweetCard