import React from 'react';
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';

import List from './List'

describe('<List />', () => {
    let wrapper, props

    beforeEach(() => {
        props = {
            tweets: [],
        };
        wrapper = shallow(<List  {...props} />);
    })

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('renders list of Grid element', () => {
        expect(wrapper.find(Grid)).toHaveLength(4);
    });  

    it('renders one LinearProgress element when tweets are 0', () => {
        expect(wrapper.find(LinearProgress)).toHaveLength(1);
    });

})
