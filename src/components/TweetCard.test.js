import React from 'react';
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import TweetCard from './TweetCard'

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';

describe('<TweetCard />', () => {
    let wrapper, props

    beforeEach(() => {
        props = {
            tweet: {
                user: { profile_image_url: 'someimage.png', name: 'Some name' },
                created_at: 'Sat Feb 02 19:06:09 +0000 2019',
                text: 'Hello word'
            },
        };
        wrapper = shallow(<TweetCard  {...props} />);
    })

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('renders one of Card element', () => {
        expect(wrapper.find(Card)).toHaveLength(1);
    });

    it('renders one of CardHeader element', () => {
        expect(wrapper.find(CardHeader)).toHaveLength(1);
    });

    it('renders one of CardContent element', () => {
        expect(wrapper.find(CardContent)).toHaveLength(1);
    });


})
