import React from 'react';
import PropTypes from "prop-types";

import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';

import ListAdapter from './ListAdapter'

const List = (props) => {

    return (
        <Grid container spacing={24}>
            <Grid item xs></Grid>
            <Grid item xs={8}>
                {
                    props.tweets.length > 0 ?
                        <ListAdapter data={props.tweets} />
                        : <LinearProgress color="secondary" />
                }
            </Grid>
            <Grid item xs></Grid>
        </Grid>
    )
}

List.propTypes = {
    tweets: PropTypes.array.isRequired,
};

export default List