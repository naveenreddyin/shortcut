import React from 'react';
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

import SearchInput from './SearchInput'

describe('<SearchInput />', () => {
    let wrapper, props, submitMock, changeMock

    beforeEach(() => {
        submitMock = changeMock = jest.fn()
        props = {
            handleSearchTermSubmit: submitMock,
            handleSearchTermChange: changeMock,
        };
        wrapper = shallow(<SearchInput  {...props} />);
    })

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('renders one InputBase element', () => {
        expect(wrapper.find(InputBase)).toHaveLength(1);
    });

    it('renders one Grid element', () => {
        expect(wrapper.find(Grid)).toHaveLength(4);
    });

    it('renders one IconButton element', () => {
        expect(wrapper.find(IconButton)).toHaveLength(1);
    });

    it('renders one SearchIcon element', () => {
        expect(wrapper.find(SearchIcon)).toHaveLength(1);
    });

    it('check if handleSearchTermSubmit event calls the prop function', () => {
        wrapper.find(IconButton).simulate('click', 'hello')
        expect(submitMock).toHaveBeenCalledWith('hello');
    })

    it('check if handleSearchTermChange change event calls the prop function', () => {
        wrapper.find(InputBase).simulate('change', 'hello')
        expect(changeMock).toHaveBeenCalledWith('hello');
    })
})
