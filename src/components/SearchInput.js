import React from 'react';
import PropTypes from "prop-types";

import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

import './styles/SearchInputStyles.css'

const SearchInput = (props) => {
    return (
        <Grid container spacing={24} className="SearchContainer">
            <Grid item xs></Grid>
            <Grid item xs={8}>
                <InputBase placeholder="Enter search term" className="SearchInput" onChange={props.handleSearchTermChange}/>
                <IconButton aria-label="Search" onClick={props.handleSearchTermSubmit}>
                    <SearchIcon />
                </IconButton>
            </Grid>
            <Grid item xs></Grid>
        </Grid>
    )
}

SearchInput.propTypes = {
    handleSearchTermSubmit: PropTypes.func.isRequired,
    handleSearchTermChange: PropTypes.func.isRequired
};

export default SearchInput;