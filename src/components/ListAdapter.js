import React, { Component } from 'react';
import PropTypes from "prop-types";

import TweetCard from './TweetCard'
import "./styles/ListAdapterStyles.css"


class ListAdapter extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data_count: 0,
            toggleFlag: false,
            tweets: [],
            temp_tweets: []
        }
    }
    /**
     * A life cycle method called after the component is mounted. We will set 
     * some of our states here with props.
     */
    componentDidMount(){
        let {data_count, temp_tweets, tweets } = this.state
        let { data } = this.props
        data_count = data.length
        temp_tweets = tweets = tweets.concat(data.slice())
        this.setState({data_count, tweets, temp_tweets})
    }
    /**
     * Another life cycle method which allows us to check and update our state based on previous props
     * data with the incoming props data.
     */
    componentDidUpdate(prevProps) {
        let { data_count, tweets } = this.state
        let { data } = this.props
        if (prevProps.data !== data) {
            
            this.setState({
                data_count: data_count === 1 ? (data.length - tweets.length) + 1: 
                (data.length - tweets.length),
                temp_tweets: data
            })
        }
    }
    /**
     * This is custom method to handle button click evet.
     */
    handleButtonClick = () => {
        let { temp_tweets } = this.state
        this.setState({ data_count: 0, 
                        toggleFlag: true, 
                        tweets: temp_tweets,
                        temp_tweets: []
                    })
    }
    /**
     * This is typical React render function
     */
    render() {
        let { data_count, tweets, toggleFlag } = this.state
        let singular_or_plural = data_count > 1 ? "tweets" : "tweet"
        return (
            <>
                <span className="TweetNumberSpan">
                    {
                        data_count > 0 ? (
                            <button className="TweetNumber" onClick={this.handleButtonClick}>
                                You have {data_count} {singular_or_plural}.
                            </button>
                        ) : ''
                    }
                    {toggleFlag &&
                        tweets.map((tweet, i) =>
                            <TweetCard key={i} tweet={tweet} />
                        )
                    }

                </span>
            </>
        )
    }
}

ListAdapter.propTypes = {
    data: PropTypes.array.isRequired,
};

export default ListAdapter