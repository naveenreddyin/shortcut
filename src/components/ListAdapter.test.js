import React from 'react';
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import ListAdapter from './ListAdapter'
import TweetCard from './TweetCard'


describe('<ListAdapter />', () => {
    let wrapper, props

    beforeEach(() => {
        props = {
            data: [{
                user: { profile_image_url: 'someimage.png', name: 'Some name' },
                created_at: 'Sat Feb 02 19:06:09 +0000 2019',
                text: 'Hello word'
            }],
        };
        wrapper = shallow(<ListAdapter  {...props} />);

    })

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('renders one link anchor element', () => {
        expect(wrapper.find('button')).toHaveLength(1);
        expect(wrapper.find(TweetCard)).toHaveLength(0);
    });

    it('check anchor tag text when it gets data and than try to set props', () => {
        expect(wrapper.find('button').at(0).text()).toEqual('You have 1 tweet.');
        let { data } = wrapper.instance().props
        data = [data].concat({
            user: { profile_image_url: 'someimage.png', name: 'Some name' },
            created_at: 'Sat Feb 02 19:06:09 +0000 2019',
            text: 'Hello word'
        })
        wrapper = wrapper.setProps({ data })
        expect(wrapper.find('button').at(0).text()).toEqual('You have 2 tweets.');
        expect(wrapper.instance().props.data).toHaveLength(2)
    });

    it('check state initialization defaults', () => {
        // we have one props getting passed so it will be 1
        expect(wrapper.state('data_count')).toEqual(1);
    });

    it('test onClick event of button', () => {
        // Try to click and than we should have toggleFlag be true and data_count be 0
        wrapper.find('button').simulate('click')
        expect(wrapper.state('data_count')).toEqual(0);
        expect(wrapper.find(TweetCard)).toHaveLength(1);
        expect(wrapper.find('button')).toHaveLength(0);
    });

})
