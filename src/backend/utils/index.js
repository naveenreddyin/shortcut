const Twitter = require('twitter');

let util = (socket) => {
    let socketConnection, streamConnection;
    let twitter = new Twitter({
        consumer_key: process.env.TWITTER_KEY,
        consumer_secret: process.env.TWITTER_SECRET,
        access_token_key: process.env.TWITTER_ACCESS_KEY,
        access_token_secret: process.env.TWITTER_ACCESS_TOKEN
    });

    socket.on("connection", io => {
        socketConnection = io;
        handleStreamChanges()
        io.on("connection", () => console.log("Client connected"));
        io.on("disconnect", () => console.log("Client disconnected"));
    });

    /**
     * Handles any changes from client when using setTerm channel to set a new term
     */
    const handleStreamChanges = () => {
        socketConnection.on('setTerm', (data) => {
            if(streamConnection !== undefined)
                streamConnection.destroy()
            getSteam(data.term)
        })
    } 

    /**
     * Get twitter stream using the twitter handler
     * @Param {String} term
     */
    const getSteam = (term) => {
        twitter.stream('statuses/filter', { track: term }, (stream) => {
            stream.on('data', (tweet) => {
                sendStream(tweet);
            });

            stream.on('error', (error) => {
                console.log(error);
            });

            streamConnection = stream;
        });
    }
    /**
     * Send the stream as emit event
     * @param {Object} data 
     */
    const sendStream = (data) => {
        if(data.retweeted_status != undefined)
            return
        socketConnection.emit('tweets', data)
    }
}

module.exports = util