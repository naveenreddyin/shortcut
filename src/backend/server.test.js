const http = require('http');
const serverSocket = require('socket.io');
const io = require('socket.io-client');

let httpServer, httpServerAddr, ioServer, socket;

/**
 * Setup WS & HTTP servers
 */
beforeAll((done) => {
    httpServer = http.createServer().listen();
    httpServerAddr = httpServer.address();
    ioServer = serverSocket(httpServer);
    done();
});

/**
 *  Cleanup WS & HTTP servers
 */
afterAll((done) => {
    ioServer.close();
    httpServer.close();
    done();
});

/**
 * Run before each test
 */
beforeEach((done) => {
    socket = io.connect(`http://[${httpServerAddr.address}]:${httpServerAddr.port}`, {
        'reconnection delay': 0,
        'reopen delay': 0,
        'force new connection': true,
        transports: ['websocket'],
    });
    socket.on('connect', () => {
        done();
    });
});

/**
 * Run after each test
 */
afterEach((done) => {
    // Cleanup
    if (socket.connected) {
        socket.disconnect();
    }
    done();
});


describe('server.js socket.io test suite', () => {
    test('should communicate', (done) => {
        // once connected, emit Hello World
        ioServer.emit('echo', 'Hello World');
        socket.once('echo', (message) => {
            // Check that the message matches
            expect(message).toBe('Hello World');
            done()
        });
        ioServer.on('connection', (mySocket) => {
            expect(mySocket).toBeDefined();
        });
        
    })
})