const http = require('http');

const app = require('express')();

// for the sake of demo let Access-Control-Allow-Origin be a wildcard, definitely not good for production
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

let server = http.createServer(app);

// for the sake of demo let origins be a wildcard, definitely not good for production
let io = require('socket.io')(server, {
    origins: '*:*',
    pingInterval: 3000,
    pingTimeout: 3000,
    cookie: false
});

require('./utils')(io);

server.listen(3001, function () {
    console.log('listening on *:3001');
});