const host = window.location.hostname

let socketSecondPart = ":3001"
let socketBaseURL = ''
let socketWSSURL = ''
if(host === 'localhost'){
  socketBaseURL = "ws://"+host+socketSecondPart+"/"
  socketWSSURL = "wss://"+host+socketSecondPart
}else{
  socketBaseURL = "http://sockets."+host;
  socketWSSURL = "wss://sockets."+host;
}

export default {
  NCP_WEBSOCKET_BASE_URL: socketBaseURL,
  UNO_WSS_BASE_URL: socketWSSURL
}