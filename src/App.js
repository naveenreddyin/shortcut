import React, { Component } from 'react';

import openSocket from 'socket.io-client';

import './App.css';
import SearchInput from './components/SearchInput'
import List from './components/List'

import config from './config'

const socket = openSocket(config.NCP_WEBSOCKET_BASE_URL);


class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      term: 'Javascript',
      tweets: [],
    }
  }
  /**
   * A lifecycle method, here we will handle connect, disconnect to server websocket as well as listen to a channel, and
   * set a term through a channel for server.
   */
  componentDidMount() {
    let { tweets } = this.state

    socket.on('connect', () => {
      console.info("connected")
      // emit the default term
      socket.emit('setTerm', { 'term': this.state.term })  
    })

    // listen to tweets
    socket.on('tweets', data => {
      tweets = [data].concat(this.state.tweets.slice());
      this.setState({ tweets })
    })

    // handle disconnect
    socket.on('disconnect', () => {
      socket.off("tweets")
      socket.removeAllListeners("tweets");
      console.log("Socket Disconnected");
    });
  }
  /**
   * A custom method which sets the value of a state
   */
  handleTermChange = (event) => {
    this.setState({ term: event.target.value })
  }
  /**
   * Handling the submit event through which we set a socket emit event to send the term entered by user.
   */
  handleSubmit = () => {
    let { term } = this.state
    socket.emit('setTerm', { 'term': term })
  }

  render() {
    let { tweets } = this.state

    return (
      <div className="App">
        <SearchInput
          handleSearchTermSubmit={this.handleSubmit}
          handleSearchTermChange={this.handleTermChange}
        />
        <List tweets={tweets} />
      </div>
    );
  }
}

export default App;
