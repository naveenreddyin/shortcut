### Introduction
* The project aims to show a demo of live streaming of Twitter data.
* Project consists of two parts, one is backend which is inside /src/backend folder and rest of the folders and files are for the frontend purposes.
* For backend the framework used is Node.js/ Express and for the frontend React.
* For realtime communication purpose, socket.io library is used.
* For the sake of simplicity of demo purposes no databases, no Redux is used
* Default tracking word is `javascript`, which could be changed by typing into the textbox on frontend page.
## Prereqsuities
* This demo assumes that nodejs is upto date and npm is upto date as well. 
* Node version `v11.9.0` and npm version `6.5.0`is used for building this project.
* For unit testing purposes `jest` and `enzyme` are used.
### Installation
* Clone the repository using `git clone ....` command
* Now go inside the dicectory `cd repository_name` and run `npm install`
* After the npm install is run, run `npm start`. This will start the frontend on port 3000.
* The frontend wont work until backend is setup too, so for that purpose, go inside backend forlder, i.e `/src/backend/` and run, depending on what you use for node server, `nodemon server.js` for installing `nodemon` run `npm install -g nodemon` or if you want to use `pm2` run `npm install -g pm2` and after this to run the backend server from `/src/backend/` folder run `pm2 start server.js` or `nodemon server.js`. This will start the server on 3001 port.
* You should be able to see the tweets now or type in your own to get related tweets.
* There are unit tests too, to run them from root folder where package.json is, run `npm run test`
### Website
* This app is hosted as a website as well for the demo purpose:
http://unogram.in